# MentalHealthReportForm_WebBrowser
Mental health report forms designed by [pForms](http://www.phpform.org/).

These are forms designed to make it easy to collect personal information, complaint, current symptoms, medical history, and family history. None of these forms meet any legal compliances ([HIPAA](http://www.onlinetech.com/resources/references/what-is-hipaa-compliance) and so forth) and were only created out of shear boredom. Though as easy as they are to use, it's a shame there isn't more ways to input mental health data.
